# Setting up a new Ubuntu PC for development

### Basic setup
Open terminal and run following command(s):


```bash
sudo apt-get update
sudo apt install curl build-essential
```

### Google Chrome

1. Go to `https://www.google.com/chrome/` and click on the 'Download Chrome' button.
2. In the modal that opens up, select the '64 bit .deb (For Debian/Ubuntu)' radio
3. Then click 'Accept and Install' and save the file in your `Downloads` directory
4. Open terminal and run the following command:
```bash
 sudo apt install ~/Downloads/google-chrome-stable*.deb
```

### VSCode

Open terminal and run following command(s):

```bash
sudo snap install --classic code
```

### VLC

Open terminal and run following command(s):

```bash
sudo snap install vlc
```

### Node.js & Yarn

Open terminal and run following command(s):

For Node.js:

```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update

sudo apt-get install -y nodejs yarn
```

### Projects directory setup

We will create a projects directory in the Home directory. This projects directory will be used to create or clone all projects/repositories.

All of your work should be in the projects directory.

Open terminal and run following command(s):

```bash
mkdir ~/EC-Projects
```

